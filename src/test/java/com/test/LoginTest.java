package com.test;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.Baseclass;
import com.page.HomePage;
import com.page.Landpage;
import com.page.PageofLogin;

public class LoginTest extends Baseclass{
	@BeforeClass
	public void setup() throws IOException
	{
		launchbrowser();
		driver.get("https://www.demoblaze.com/index.html");
	}
	@AfterClass
	public void setdown()
	{
		driver.close();
	}
	@Test(priority = 1)
	public void verifytitle()
	{
		String actualtitle = driver.getTitle();
		String exectedTitle="STORE";
		Assert.assertEquals(actualtitle, exectedTitle);
		
		
	}
	@Test(priority = 2)
	public void verifylogin()
	{
		Landpage ld=new Landpage();
		ld.getBtn().click();
		PageofLogin pl=new PageofLogin();
		pl.getuser().sendKeys("test.selenium@gmail.com");
		pl.getpass().sendKeys("Test@selenium");
		pl.getlogBtn().click();
				
	}
	@Test(priority = 3)
	public void verifyMessage()
	{
		HomePage hp=new HomePage();
		String actualText = hp.getMessage().getText();
		String expectedText="Welcome test.selenium@gmail.com";
		Assert.assertEquals(actualText, expectedText);
	}
	
}
