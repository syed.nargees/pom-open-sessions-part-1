package com.base;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Baseclass {
	public static WebDriver driver;
	public static Properties prop;
	public void launchbrowser() throws IOException
	{
		prop=new Properties();
		FileInputStream f=new FileInputStream("C:\\Users\\syed.nargees\\eclipse-workspace\\POMsession\\PomDemoON\\src\\main\\java\\com\\properies\\config.properties");
		prop.load(f);
		String nameofbrowser = prop.getProperty("browser1");
		if(nameofbrowser.equals("chrome"))
		{
			driver=new ChromeDriver();
			
		}
		else if(nameofbrowser.equals("firefox"))
		{
			driver= new FirefoxDriver();
		}
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
	}
}
