package com.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.base.Baseclass;

public class PageofLogin extends Baseclass{
	//WebDriver driver;
	private By usertext=By.id("loginusername");
	private By passtext=By.id("loginpassword");
	private By loginbtn=By.xpath("//button[text()='Log in']");
	public WebElement getuser()
	{
		return driver.findElement(usertext);
		
	}
	public WebElement getpass()
	{
		return driver.findElement(passtext);
		
	}
	public WebElement getlogBtn()
	{
		return driver.findElement(loginbtn);
	}
	
}
