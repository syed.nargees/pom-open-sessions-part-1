package com.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.base.Baseclass;

public class HomePage extends Baseclass {
	private By message=By.linkText("Welcome test.selenium@gmail.com");
	public WebElement getMessage()
	{
		return driver.findElement(message);
	}
	
}
